import 'package:flutter/material.dart';

class DictionaryPage extends StatelessWidget {
  const DictionaryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: Theme.of(context).textTheme.bodyText2!,
      child: Column(children: <Widget>[
        const Padding(
          padding: EdgeInsets.all(16.0),
          child: Text('DEUTSCH'),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: const [
              Text('Wie heisst du?'),
              Text('Hvad er dit navn?'),
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.all(16.0),
          child: Text('Ich heisse...'),
        ),
        const Padding(
          padding: EdgeInsets.all(16.0),
          child: Text('Ich mag...'),
        ),
        const Padding(
          padding: EdgeInsets.all(16.0),
          child: Text('Wie viel kostet das?'),
        ),
        const Padding(
          padding: EdgeInsets.all(16.0),
          child: Text('Wo ist...'),
        )
      ]),
    );
  }
}
