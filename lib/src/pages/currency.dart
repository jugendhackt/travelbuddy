import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:frankfurter/frankfurter.dart';

class CurrencyPage extends StatefulWidget {
  const CurrencyPage({Key? key}) : super(key: key);

  @override
  State<CurrencyPage> createState() => _CurrencyPageState();
}

class _CurrencyPageState extends State<CurrencyPage> {
  String? _currencyFrom = "EUR";
  String? _currencyTo = "DKK";
  double _exchange = 0;

  final _fromController = TextEditingController(text: "10");
  final _toController = TextEditingController();

  _fetchExchange() async {
    if (mounted) {
      try {
        final rate = await Frankfurter().getRate(
            from: Currency(_currencyFrom!), to: Currency(_currencyTo!));
        setState(() {
          _exchange = rate.rate;
        });
        _toController.text =
            (double.parse(_fromController.text) * _exchange).toString();
      } catch (e) {
        _toController.text = "";
      }
    }
  }

  @override
  void initState() {
    _fromController.addListener(_fetchExchange);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 256),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 64),
        child: ListView(
          children: [
            TextField(
              controller: _fromController,
              decoration: InputDecoration(
                prefixIcon: CurrencyDropdown(
                  onChanged: (String? value) {
                    setState(() {
                      _currencyFrom = value;
                    });

                    _fetchExchange();
                  },
                  value: _currencyFrom!,
                ),
                border: const OutlineInputBorder(),
                labelText: "Ausgangswährung",
              ),
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            ),
            TextField(
              controller: _toController,
              decoration: InputDecoration(
                prefixIcon: CurrencyDropdown(
                  onChanged: (String? value) {
                    setState(() {
                      _currencyTo = value;
                    });

                    _fetchExchange();
                  },
                  value: _currencyTo!,
                ),
                border: const OutlineInputBorder(),
                labelText: "Zielwährung",
              ),
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            ),
            const Center(
              child: Text("Wechselkurs"),
            ),
            Center(child: Text("1 $_currencyFrom = $_exchange $_currencyTo")),
          ]
              .map((e) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: e,
                  ))
              .toList(),
        ),
      ),
    );
  }
}

class CurrencyDropdown extends StatelessWidget {
  static const supportedCurrencies = [
    "EUR",
    "DKK",
    "USD",
  ];

  final ValueChanged<String?> onChanged;

  final String value;

  const CurrencyDropdown(
      {super.key, required this.onChanged, required this.value});

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
        items: supportedCurrencies
            .map((e) => DropdownMenuItem(
                  value: e,
                  child: Text(e),
                ))
            .toList(),
        value: value,
        onChanged: onChanged);
  }
}
